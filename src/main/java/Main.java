import jpoint.LineSqueezer;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        LineSqueezer lineSqueezer = new LineSqueezer();
        System.out.println("Введите строку");
        System.out.println(lineSqueezer.squeezeLine(input.nextLine()));
    }

}