package jpoint;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Реализация {@link LineSqueezer}
 */
public class LineSqueezer {

    public String squeezeLine(String line) {
        if (line == null) {
            return null;
        }

        List<String> currentLine = Arrays.asList(line.split(""));
        StringBuilder resultLine = new StringBuilder();

        for (int i = 0; i < currentLine.size(); ) {
            String currentItem = currentLine.get(i);
            int count = Collections.frequency(currentLine, currentItem);
            i += count;
            if (count != 1) {
                resultLine.append(count);
            }
            resultLine.append(currentItem);
        }
        return resultLine.toString();
    }

}